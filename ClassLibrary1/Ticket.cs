﻿using System;

namespace ClassLibrary1
{
    public class Ticket
    {
        public int Id { get; set; }
        public Buses Bus { get; set; }
        public string SeatNumber { get; set; }
        public decimal Price { get; set; }

        public Ticket(int id, Buses bus, string seatNumber, decimal price)
        {
            Id = id;
            Bus = bus;
            SeatNumber = seatNumber;
            Price = price;
        }
    }
}
